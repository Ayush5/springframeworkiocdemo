package com.esewa.springframeworkiocdemo;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

public class Main {
    public static void main(String[] args) {

        // Using Bean Factory
        Resource resource = new FileSystemResource("src/resources/applicationContext.xml");
        BeanFactory bf=new XmlBeanFactory(resource);
        Student student = (Student) bf.getBean("studentbean");
        System.out.println(student.getId() + " " + student.getName());


        // Using Application Context
        ApplicationContext context = new ClassPathXmlApplicationContext("resources/applicationContext.xml");
        Student student1 = (Student) context.getBean("studentbean");
        System.out.println(student1.getId() + " " + student1.getName());


    }
}

